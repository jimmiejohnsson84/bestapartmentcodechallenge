#include <iostream>
#include <vector>
#include <map>

/*

I want to find the index in blocks which minimizes the total sum distance to all my different reqs.

Follow up question: Is any of the reqs more important or are they all the same important?
- Lets assume they are all equally important.

Could there be a setup where one of the reqs are missing?
- Lets assume all items in reqs can be found.

What I want to do is go through each block and evalute the total distance to all my reqs from that block.

vector<unsigned int> blockScores; // Lower is better
vector<string> reqs;
*/

// Remove to disable debug prints
#define SHOW_DEBUG_PRINTS

int findBlockScores(const std::vector<std::map<std::string, bool>> &inputBlocks, const std::vector<std::string> &reqs)
{
	using namespace std;

	vector<unsigned int> blockScores;
	blockScores.resize(inputBlocks.size());

	// I wanna keep track of the index here so lets use a regular for loop
	for(int i = 0; i < inputBlocks.size(); i++)
	{
		vector<string> reqsLeft;
		// Calcualte the score for this block
		for(auto req : reqs)
		{
			// Found at this block
			if(inputBlocks[i].find(req)->second)
			{
				blockScores[i] += 0;
			}
			else
			{
				reqsLeft.push_back(req);
			}
		}

		#ifdef SHOW_DEBUG_PRINTS
			cout << "Reqs left at block[" << i << "]: ";		
			for(auto req : reqsLeft)
			{
				cout << req << ", ";
			}
			cout << endl;
		#endif

		// Things that we could not reach from this block.
		for(auto req : reqsLeft)
		{
			int j = 0;
			int smallestNext = 0;
			bool foundOne = false;
			while(j < inputBlocks.size())
			{
				if(j != i)
				{
					if(inputBlocks[j].find(req)->second)
					{
						int curr = 0;
						if(j < i)
						{							
							curr = i - j;
						}
						else
						{
							curr = j - i;
						}
						if(!foundOne)
						{
							smallestNext = curr;
							foundOne = true;
						}
						else
						{
							if(curr < smallestNext)
							{
								smallestNext = curr;
							}
						}						
					}
				}
				j++;
			}

			#ifdef SHOW_DEBUG_PRINTS
				cout << "Found nearest " << req << " dist: " << smallestNext << endl;
			#endif

			blockScores[i] += smallestNext;
		}
	}

	#ifdef SHOW_DEBUG_PRINTS
		// Debug outprint - show all block scores
		for(int i = 0; i < blockScores.size(); i++)
		{
			cout << "block[" << i << "] : " << blockScores[i] << endl;
		}
	#endif

	int smallestPossible = blockScores[0];
	int smallestPossibledIndex = 0;

	for(int i = 1; i < blockScores.size(); i++)
	{
		if(blockScores[i] < smallestPossible)
		{
			smallestPossible = blockScores[i];
			smallestPossibledIndex = i;
		}
	}

	return smallestPossibledIndex;
}


/* 
	Code interview question:

	https://youtu.be/rw4s4M3hFfs


	You are looking for an apartment to rent. There are a couple of different things that you want to have close by to yourself
	(perhaps a gym, school, office etc.)

	You want to optimze the apartment you choose you want to look for by looking for an apartment that is as close as possible
	to all the different things you want to be close by.

	You will be given a list of dictionaries that describe if something exists or not at a block.
	You will also be given a list of requirements.

	Example of some follow up questions:
	- Are all the requirements equally important?
		: Lets assume that yes, they are all equally important.

	- Could there be a setup where one of the requirements we are looking for is missing?
		: Lets assume no.

	- There could be any number of given requirements.

	- Each entry in block has a true/false value for all the items in the requirements list.
	
	Example input:

	blocks  = [
	{
		gym:false
		school:true
		store:false
	},
	{
		gym:true
		school:false
		store:false
	},
	{
		gym:true
		school:true
		store:false
	},
	{
		gym:false
		school:true
		store:false
	},
	{
		gym:false
		school:true
		store:true
	},

	]

	reqs = [gym, school, store]

	Expected output:
	Index 3
	(
		{
			gym:false
			school:true
			store:false
		},
	)

*/

void debugPrintBlocks(const std::vector<std::map<std::string, bool>> &blocks);

// Driver code
int main()
{
	using namespace std;
	vector<map<string, bool>> blocks = 
	{
		{
			{"gym", false}, 
			{"school", true},
			{"store", false}
		},
		{
			{"gym", true}, 
			{"school", false},
			{"store", false}
		},
		{
			{"gym", true}, 
			{"school", true},
			{"store", false}
		},
		{
			{"gym", false}, 
			{"school", true},
			{"store", false}
		},
		{
			{"gym", false}, 
			{"school", true},
			{"store", true}
		}
	};

	vector<string> reqs = {"gym", "school", "store" };

	#ifdef SHOW_DEBUG_PRINTS
		debugPrintBlocks(blocks);
		cout << "reqs: [";
		for(auto req: reqs)
		{
			cout << req << ", " ;
		}
		cout << "]" << endl;
		cout << endl;
	#endif

	int bestBlock = findBlockScores(blocks, reqs);

	cout << "Best block: " << bestBlock << endl;
}

void debugPrintBlocks(const std::vector<std::map<std::string, bool>> &blocks)
{
	using namespace std;

	for(auto block : blocks)
	{
		auto it = block.begin();
		cout << "{" << endl;
		while(it != block.end())
		{
			cout << "	" << it->first << " : " << it->second << endl;
			it++;
		}
		cout << "}," << endl;
	}
}